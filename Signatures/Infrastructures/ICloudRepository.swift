//
//  ICloudRepository.swift
//  Reader
//
//  Created by Masatoshi Nishikata on 15/12/16.
//
//

import Foundation

typealias ICloudConflictDecisionHandler = (_ fileVersion: NSFileVersion) -> Void
typealias ICloudConflictNotificationHandler = (_ allVersions: [NSFileVersion], _ decisionHandler: @escaping ICloudConflictDecisionHandler ) -> Void


final class ICloudRepository {
  
  //CONST
  static let lastUpdateTimeStamp = "iCloudDidChangeLastUpdateTimeStamp"
  static let indexPlistTimeStamp = "ICloudIndexPlistTimeStamp"

  static let ICloudDocumentFileName = "SharedDefault.realm"
  
  private var _query: NSMetadataQuery? = nil
  var query: NSMetadataQuery! {
    return _query
  }
  var cloudDidUpdateHandler: ((_ url: URL)->Void)?
  var conflictHandler: ICloudConflictNotificationHandler?
  var errorHandler: ((_ error:Error) -> Void)?
  
  var iCloudReady = false {
    didSet {
      print("*** iCloud Ready ***")
    }
  }

  static var shared = ICloudRepository()

  func startICloudQuery() {

    guard let _ = FileManager.default.ubiquityIdentityToken else { return }

    // Document folder observer
    if _query == nil {
      
      _query = NSMetadataQuery()
      _query!.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
      _query!.enableUpdates()
      
      let predicate = NSPredicate(format: "%K LIKE %@", NSMetadataItemFSNameKey , ICloudRepository.ICloudDocumentFileName)
      _query!.predicate = predicate
      
      let center = NotificationCenter.default
      center.addObserver(self, selector: #selector(cloudDidChange), name: .NSMetadataQueryDidUpdate, object: nil)
      center.addObserver(self, selector: #selector(cloudDidChange), name: .NSMetadataQueryDidFinishGathering, object: nil)
      
      _query!.start()
      
    }
  }
  
  @objc func cloudDidChange() {
    
    let results = _query!.results
    
    if results.count == 0 {
      print("*** New for iCloud ***")
      
      iCloudReady = true
      return /* First time to use*/
    }
    
    print("- iCloud Loading")
    
    let metadata = results[0] as! NSMetadataItem
    
    guard let url = metadata.value(forAttribute: NSMetadataItemURLKey) as? URL else { return }
    
    var modificationDate = Date.distantPast
    
    let status = metadata.value(forAttribute: NSMetadataUbiquitousItemDownloadingStatusKey) as? String
    if status == NSMetadataUbiquitousItemDownloadingStatusNotDownloaded {
      
      do {
        try FileManager.default.startDownloadingUbiquitousItem(at: url)
      } catch let error1 as NSError {
        errorHandler?(error1)
      }
      return
    }
    
    
    print("- iCloud Loading")
    let ud = UserDefaults.standard
    
    if status != NSMetadataUbiquitousItemDownloadingStatusCurrent { return } // Not Up to date
    
    if metadata.value(forAttribute: NSMetadataItemFSContentChangeDateKey) != nil {
      modificationDate = metadata.value(forAttribute: NSMetadataItemFSContentChangeDateKey) as! Date
    }
    
    print("*** iCloud Changed ***")
    print("modificationDate \(modificationDate)")
    
    let allVersions = NSFileVersion.unresolvedConflictVersionsOfItem(at: url)
    
    if allVersions == nil ||  allVersions!.count == 0  {
      let lastTimeStamp = ud.double(forKey: ICloudRepository.lastUpdateTimeStamp)
      
      print("lastTimeStamp \(lastTimeStamp) - \(modificationDate.timeIntervalSinceReferenceDate)")
      
      if lastTimeStamp >= modificationDate.timeIntervalSinceReferenceDate {
        ICloudRepository.shared.iCloudReady = true
        return
      }
      else {
        ud.set( modificationDate.timeIntervalSinceReferenceDate, forKey: ICloudRepository.lastUpdateTimeStamp )
      }
    }
    
    if allVersions != nil && allVersions!.count > 0 {

      let currentVersion = NSFileVersion.currentVersionOfItem(at: url)!
      
      /*
       
       Call conflict handler with allVersions and decision handler that sends fileVersion to use
       
       */
      
      print("call conflict handler \(conflictHandler)")
      
      let decisionHandler: ICloudConflictDecisionHandler = { (fileVersion: NSFileVersion) -> Void in
        
        if fileVersion === currentVersion {
          
          var success: Bool
          do {
            try NSFileVersion.removeOtherVersionsOfItem(at: url)
            success = true
            
            for aVersion in allVersions! {
              try aVersion.remove()
            }
            
          } catch let error1 as NSError {
            self.errorHandler?(error1)
            success = false
          }
          
          print("removeOtherVersionsOfItemAtURL \(success)")
          
        }else {
          
          var success: URL?
          do {
            
            // Keep current backup
            #if os(iOS)
              //FilePackage.shared.keepBackupForFileAt(theURLOrMyDocumentWhenNull: nil, forceBackup: true)
            #endif
            
            success = try fileVersion.replaceItem(at: url, options: .byMoving)
            
            for aVersion in allVersions! {
              if aVersion !== fileVersion {
                try aVersion.remove()
              }
            }
            
            ud.set( 0, forKey: ICloudRepository.indexPlistTimeStamp )
            
          } catch let error1 as NSError {
            self.errorHandler?(error1)
            success = nil
          }
          print("replaceItemAtURL \(success)")
        }
        
        self.cloudDidUpdateHandler?(url)

        //loadFromICloudFile(at: url)
        //ICloudRepository.shared.iCloudReady = true
      }
      
      self.conflictHandler!( ( [currentVersion] + allVersions!), decisionHandler)
      
    }else {
      
      cloudDidUpdateHandler?(url)
      
    }
  }
  
  func saveToICloud() {
    let fm = FileManager.default
    let ud = UserDefaults.standard
    
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
      
      let token = fm.ubiquityIdentityToken
      if token == nil { return }
      
      let url = fm.url(forUbiquityContainerIdentifier: nil)
      if url == nil { return } // iCloud is off
      
      let iCloudDocumentFolder = url!.appendingPathComponent("Documents")
      
      if fm.fileExists(atPath: iCloudDocumentFolder.path) == false {
        do {
          try fm.createDirectory(at: iCloudDocumentFolder, withIntermediateDirectories: true, attributes: nil)
        } catch _ {
        }
      }
      
      let indexPlistURL = iCloudDocumentFolder.appendingPathComponent(ICloudRepository.ICloudDocumentFileName)
      
      let coordinator = NSFileCoordinator()
      coordinator.coordinate(writingItemAt: indexPlistURL, options: .forReplacing, error: nil) { (newURL: URL) -> Void in
        
        let timeStamp = Date.timeIntervalSinceReferenceDate
        
        do {
          try RealmRepository.shared.writeRecordRealmCopy(toFile: newURL)
          
        }catch let err {
          debugPrint("** err \(err.localizedDescription)")
        }
        
        // Get modification date
        do {
          let attributes = try FileManager.default.attributesOfItem(atPath: newURL.path)
          if let modificationDate = attributes[FileAttributeKey.modificationDate] as? Date {
            
            ud.set( modificationDate.timeIntervalSinceReferenceDate,
                    forKey: ICloudRepository.lastUpdateTimeStamp )
            
          }
          
          ud.set( timeStamp, forKey: ICloudRepository.indexPlistTimeStamp )
          
        } catch {
          
        }
      }
    }
  }
}

