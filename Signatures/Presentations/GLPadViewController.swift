//
//  GLPadViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

protocol GLPadViewControllerDelegate: NSObjectProtocol {
  func padViewControllerWillClose(_ record: Record)
}

class GLPadBaseClass: UIViewController, WritepadInputViewDelegate {
  
  @IBOutlet weak var glView: WritepadPolygonGLView!
  weak var delegate: GLPadViewControllerDelegate?

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    glView.lineWidth = 10.0
    glView.lineHeight = 120
    glView.strokeType = MNStrokeTypeFude
    glView.isDoubleRow = false
    glView.drawsBaselines = false
    glView.delegate = self
    
    navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }

  func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, touchBegan touch: TouchHistory!, event: UIEvent!) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, touchEnded touch: TouchHistory!, event: UIEvent!, isDoubleTap doubleTap: Bool) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, willDrawLines polygonDraw: PolygonDraw!, points: UnsafeMutablePointer<LinePoint>!, count: UInt) {
  }
  
  func pendingTanzaku(_ tanzaku: MNTanzaku) {
  }
  
  func backgroundColor() -> MyColor {
    let color = MyColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    return color
  }
  
  func convertDot() -> Bool {
    return true
  }
  
  func convertDotsType() -> Int {
    return 0
  }
  
  func enter(_ tanzaku:MNTanzaku) {
  }
}


class GLPadViewController: GLPadBaseClass {
  
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var clearButton: UIButton!

  var drawData = DrawData()

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let tanzaku = drawData.printTanzaku
    glView.clear(self)
    glView.push(tanzaku)
  }
  
  @IBAction func clear(_ sender: Any) {
    glView.clear(self)
    nextButton.isEnabled = false
    clearButton.isEnabled = false
  }
  @IBAction func cancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    glView.enter(self)
    if let controller = segue.destination as? GLPadViewController2 {
      controller.drawData = drawData
      controller.delegate = delegate
    }
  }
  
  override func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
    nextButton.isEnabled = true
    clearButton.isEnabled = true
  }
 
  override func enter(_ tanzaku:MNTanzaku) {
    drawData.printTanzaku = tanzaku
  }
}

class GLPadViewController2: GLPadBaseClass {
  
  var drawData: DrawData!
  
  @IBOutlet weak var doneButton: UIButton!
  @IBOutlet weak var clearButton: UIButton!

  @IBAction func clear(_ sender: Any) {
    glView.clear(self)
    doneButton.isEnabled = false
    clearButton.isEnabled = false
  }
  
  @IBAction func cancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func done(_ sender: Any) {
    glView.enter(self)
    var success = false
    defer {
      if success == false {
        ShowAlertMessage("Error", "Could not save", self)
      }
    }
    
    guard let print = drawData.printTanzaku else { return }
    guard let signature = drawData.signatureTanzaku else { return }
    
    let record = Record()
    guard let printData = try? PropertyListSerialization.data(fromPropertyList: print.dictionary(), format: .binary, options: 0) else { return }
    record.printName = printData
    guard let signData = try? PropertyListSerialization.data(fromPropertyList: signature.dictionary(), format: .binary, options: 0) else { return }
    record.signature = signData
    
    success = true
    delegate?.padViewControllerWillClose(record)
    dismiss(animated: true, completion: nil)
  }

  override func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
    doneButton.isEnabled = true
    clearButton.isEnabled = true
  }
  
  override func enter(_ tanzaku:MNTanzaku) {
    drawData.signatureTanzaku = tanzaku
  }
  
}
