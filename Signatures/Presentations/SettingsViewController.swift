//
//  SettingsViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 24/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation


class SettingsViewController: UIViewController {
  
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var sortSwitch: UISwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.preferredContentSize = CGSize(width:320, height: 300)
  }
  
  @IBAction func sortChanged(_ sender: Any) {
    UserDefaultsStorage().descendingSortOrder = !sortSwitch.isOn
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    sortSwitch.isOn = !UserDefaultsStorage().descendingSortOrder
    emailField.text = UserDefaultsStorage().defaultEmail
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    UserDefaultsStorage().defaultEmail = emailField.text
  }
}
