//
//  ListTableViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit
import RealmSwift
import Messages
import MessageUI

class ListTableViewController: UITableViewController {
  
  var listPresenter = ListUtils()
  
  func reload() {
    tableView.reloadData()
  }
  
  @IBAction func export(_ sender: Any) {
    guard MFMailComposeViewController.canSendMail() == true else { return }
    guard let attachmentUrl = listPresenter.pdf() else { return }
    let controller = MFMailComposeViewController()
    controller.modalPresentationStyle = .formSheet
    guard let data = try? Data(contentsOf: attachmentUrl) else { return }
    controller.addAttachmentData(data, mimeType: "application/pdf", fileName: "Document.pdf")
    controller.setToRecipients(UserDefaultsStorage().defaultEmail?.components(separatedBy: ","))
    controller.mailComposeDelegate = self
    present(controller, animated: true, completion: nil)
  }
  
  @IBAction func edit(_ sender: Any) {
    self.setEditing(!self.isEditing, animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.isToolbarHidden = false
    
    NotificationCenter.default.addObserver(self, selector: #selector(reload), name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.removeObserver(self, name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  @IBAction func add() {
    
    if let nav = self.storyboard?.instantiateViewController(withIdentifier: "GLPadNavigationController") as! UINavigationController? {
      if let controller = nav.viewControllers.first as? GLPadViewController {
        controller.delegate = self
      self.navigationController?.present(nav, animated: true, completion:nil)
      }
    }
    
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return DataSource.shared.numberOfRecords
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 75
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecordCell
    
    guard DataSource.shared.numberOfRecords > indexPath.row else { return cell }
    let record: Record = DataSource.shared.record(at: indexPath.row)
    cell.timestampLabel.text = listPresenter.dateString(from: record.timestamp as Date)

    if let button = cell.checkButton {
      button.action = { [unowned button] in
        button.isSelected = !button.isSelected
        DataSource.shared.updateRecord(record, checked: button.isSelected)
      }
      
      button.tag = indexPath.row
      button.setTitle(nil, for: .normal)
      button.setImage(listPresenter.checkImage(false), for: .normal)
      button.setImage(listPresenter.checkImage(true), for: .selected)
      button.isSelected = record.checked
    }
    
    if let tanzaku = MNTanzaku(from: record.printName) {
        cell.imageView1.tanzaku = tanzaku
    }
    
    if let tanzaku = MNTanzaku(from: record.signature) {
      cell.imageView2.tanzaku = tanzaku
    }
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    guard DataSource.shared.numberOfRecords > indexPath.row else { return }

    DataSource.shared.deleteRecord(at: indexPath.row)
    tableView.deleteRows(at: [indexPath], with: .automatic)
  }
  
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
}

extension ListTableViewController: MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    
    controller.dismiss(animated: true, completion: nil)
  }
}

extension ListTableViewController: GLPadViewControllerDelegate {

  func padViewControllerWillClose(_ record: Record) {
    DataSource.shared.addRecord(record)
    reload()
  }
}
