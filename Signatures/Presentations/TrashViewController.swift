//
//  TrashViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 24/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

class TrashViewController: UIViewController {
  
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var confirmButton: UISwitch!
  override func viewDidLoad() {
    super.viewDidLoad()
    
    okButton.isEnabled = false
    self.preferredContentSize = CGSize(width:320, height: 200)
  }
  
  @IBAction func deleteAction(_ sender: Any) {
    let controller = UIAlertController(title: "Delete All?", message: "", preferredStyle: .alert)
    let action = UIAlertAction(title: "Delete", style: .destructive) { action in
      DataSource.shared.deleteAll()
    }
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    controller.addAction(action)
    controller.addAction(cancel)
    let parent = self.presentingViewController
    dismiss(animated: true) {
      parent!.present(controller, animated: true, completion: nil)
    }
  }
  
  @IBAction func confirmAction(_ sender: Any) {
    okButton.isEnabled = confirmButton.isOn
  }

}
