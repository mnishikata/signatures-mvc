//
//  UserDefaultsStorage.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation

class UserDefaultsStorage {
  
  static let defaultEmail = "defaultEmail"
  static let descendingSortOrder = "descendingSortOrder"
  
  
  var descendingSortOrder: Bool {
    get {
      return UserDefaults.standard.bool(forKey: UserDefaultsStorage.descendingSortOrder)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: UserDefaultsStorage.descendingSortOrder)
      NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)
      
    }
  }
  
  var defaultEmail: String? {
    get {
      return UserDefaults.standard.object(forKey: UserDefaultsStorage.defaultEmail) as? String
    }
    set {
      if newValue == nil {
        UserDefaults.standard.removeObject(forKey: UserDefaultsStorage.defaultEmail)
      }else {
        UserDefaults.standard.set(newValue!, forKey: UserDefaultsStorage.defaultEmail)
      }
    }
  }
}
