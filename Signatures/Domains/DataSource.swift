//
//  DataSource.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation
import RealmSwift

// NOTIFICATION

enum DataSourceNotification {
  static let selectionDidChange = Notification.Name("DataSourceSelectionDidChange")
  static let setNeedsDisplay = Notification.Name("DataSourceSetNeedsDisplay")
}

final class DataSource: NSObject {
  
  //MARK:-
  static let shared = DataSource()
  var errorHandler: ((_ error:Error) -> Void)?

  override init() {
    super.init()
    let iCloudRepo = ICloudRepository.shared
    iCloudRepo.cloudDidUpdateHandler = { url in self.cloudDidChange(at: url) }
    iCloudRepo.conflictHandler = { ( allVersions: [NSFileVersion], decisionHandler: @escaping ICloudConflictDecisionHandler) in
      print("### Handling Conflicts ###")
      decisionHandler(allVersions.first!)
    }
    iCloudRepo.errorHandler = errorHandler // TODO
    iCloudRepo.startICloudQuery()
  }

  
  //MARK:- ICLOUD DOCUMENT
  
  func cloudDidChange(at url: URL) {
    
    RealmRepository.shared.mingleRecords(from: url) {
      NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)
    }
  }

  //MARK: - Record
  
  func addRecord(_ record: Record) {
    RealmRepository.shared.add(record)
    ICloudRepository.shared.saveToICloud()
  }
  
  func updateRecord(_ record: Record, checked: Bool) {
    RealmRepository.shared.update(record, checked: checked)
    ICloudRepository.shared.saveToICloud()
  }
  
  func deleteRecord(at index: Int) {
    let record = self.record(at: index)
    RealmRepository.shared.delete(record)
    ICloudRepository.shared.saveToICloud()
  }
  
  func record(at index: Int) -> Record {
    let record = RealmRepository.shared.records[index]
    return record
  }
  
  var numberOfRecords: Int {
    return RealmRepository.shared.records.count
  }
 
  func deleteAll() {
    RealmRepository.shared.deleteAll()
    ICloudRepository.shared.saveToICloud()
    
    NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil) // NOT CALLED??
  }
}
