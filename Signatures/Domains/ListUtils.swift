//
//  ListUtils.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//

import UIKit
import RealmSwift

class ListUtils
{
  func pdf() -> URL? {
    let records = RealmRepository.shared.records
    let height: CGFloat = 50
    let documentWidth: CGFloat = 768
    let tanzakuWidth: CGFloat = 200
    let size = CGSize(width: documentWidth, height: CGFloat(records.count) * height )
    let pathBoundingBox = CGRect(origin:CGPoint.zero, size:size)
    let folder = URL(fileURLWithPath: NSTemporaryDirectory())
    let destUrl = folder.appendingPathComponent("Document.pdf")
    
    if FileManager.default.fileExists(atPath: destUrl.path) {
      try? FileManager.default.removeItem(at: destUrl)
    }
    
    UIGraphicsBeginPDFContextToFile (
      destUrl.path,
      pathBoundingBox,
      nil
    )
    
    guard let ctx = UIGraphicsGetCurrentContext() else { return nil }
    ctx.setLineCap(.round)
    ctx.setLineJoin(.round)
    
    UIGraphicsBeginPDFPage()
    
    var y: CGFloat = 0
    for record in records {
      autoreleasepool {
        var x: CGFloat = 5
        let checked = record.checked
        guard let date = record.creationDate else { return }
        guard let print = MNTanzaku(from: record.printName) else { return }
        guard let signature = MNTanzaku(from: record.signature) else { return }
        
        var rect = CGRect(x: x, y: y, width: tanzakuWidth, height: height)
        print.draw(in: rect, in: ctx)
        x += tanzakuWidth
        
        rect = CGRect(x: x, y: y, width: tanzakuWidth, height: height)
        signature.draw(in: rect, in: ctx)
        
        x += tanzakuWidth
        
        let dateString = self.dateString(from: date as Date)
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.black]
        let attrString = NSAttributedString(string: dateString, attributes: attributes)
        
        attrString.draw(at: CGPoint(x: x, y: y + (height - attrString.size().height)/2))
        
        x += attrString.size().width
        
        let checkImage = self.checkImage(checked)
        checkImage.draw(in: CGRect(x: x + 5, y: y + 5, width: height - 10, height: height - 10))
        
        y += height
        
        let points = [CGPoint(x: 5, y: y - 0.5), CGPoint(x: documentWidth - 5, y: y - 0.5)]
        ctx.setLineWidth(0.5)
        ctx.setStrokeColor(UIColor.lightGray.cgColor)
        ctx.strokeLineSegments(between: points)
      }
    }
    
    UIGraphicsEndPDFContext()
    return destUrl
  }

  func checkImage(_ checked: Bool) -> UIImage {
    let checkImage = checked ? #imageLiteral(resourceName: "MNOutlineChecked") : #imageLiteral(resourceName: "MNOutlineUnchecked")
    return checkImage
  }
  
  lazy var dateFormatter :DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    return formatter
  }()
  
  func dateString(from date: Date) -> String {
    return dateFormatter.string(from: date)
  }
  
}
