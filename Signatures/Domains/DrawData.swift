//
//  DrawData.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 30/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation

struct DrawData {
  var printTanzaku: MNTanzaku!
  var signatureTanzaku: MNTanzaku!
}
