//
//  SignaturesTests
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import XCTest
@testable import Signatures

class SignaturesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSavingPdfOnDesktop() {

      guard let pdfUrl = DataSource.shared.pdf() else { assert(false) }
      let desktop = URL(fileURLWithPath: "/Users/Masa/Desktop/Test.pdf")
      if FileManager.default.fileExists(atPath: desktop.path) {
        try?FileManager.default.removeItem(at: desktop)
      }
      
      do {
        try FileManager.default.copyItem(at: pdfUrl, to: desktop )
      }catch let error as NSError {
       assert( false, error.localizedDescription)
      }
  }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
